# Generated by Django 2.1.1 on 2018-09-23 06:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('courseinfo', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='course',
            options={'ordering': ['course_number', 'course_name']},
        ),
        migrations.AlterModelOptions(
            name='instructor',
            options={'ordering': ['last_name', 'first_name']},
        ),
        migrations.AlterModelOptions(
            name='registration',
            options={'ordering': ['section', 'student']},
        ),
        migrations.AlterModelOptions(
            name='section',
            options={'ordering': ['course__course_number', 'section_name', 'semester__semester_name']},
        ),
        migrations.AlterModelOptions(
            name='semester',
            options={'ordering': ['semester_name']},
        ),
        migrations.AlterModelOptions(
            name='student',
            options={'ordering': ['last_name', 'first_name', 'nickname']},
        ),
        migrations.AlterField(
            model_name='registration',
            name='section',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='registrations', to='courseinfo.Section'),
        ),
        migrations.AlterUniqueTogether(
            name='course',
            unique_together={('course_number', 'course_name')},
        ),
        migrations.AlterUniqueTogether(
            name='registration',
            unique_together={('section', 'student')},
        ),
        migrations.AlterUniqueTogether(
            name='student',
            unique_together={('last_name', 'first_name', 'nickname')},
        ),
    ]
